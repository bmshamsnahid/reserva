const express = require('express');
const router = express.Router();
const shopOwnerInfoController = require('../../controller/shopOwner/shopOwnerInfo');

router.post('/shopOwnerDataTable', shopOwnerInfoController.getShopOwnerDataTable);
router.get('/shopOwnerApproval/:id', shopOwnerInfoController.changeShopOwnerApproval);

module.exports = router;
