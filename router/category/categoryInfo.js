const express = require('express');
const router = express.Router();
const categoryController = require('../../controller/category/categoryInfo');

router.post('/', categoryController.createCategory);
router.get('/', categoryController.getCategories);
router.get('/:id', categoryController.getCategory);
router.patch('/:id', categoryController.updateCategory);
router.delete('/:id', categoryController.deleteCategory);

router.post('/categoryDataTable', categoryController.getAllCategoryData);

module.exports = router;
