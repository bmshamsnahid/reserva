const express = require('express');
const router = express.Router();
const { UserInfo } = require('../../../model/user/userInfo');

router.get('/createShopOwnerDisplay', (req, res, next) => {
    return res.render('shopOwner/createShopOwner');
});

router.get('/updateShopOwnerDisplay', (req, res, next) => {
    return res.render('shopOwner/updateShopOwner');
});

router.get('/shopOwnerListDisplay', (req, res, next) => {
    return res.render('shopOwner/shopOwnerList');
});

router.get('/approveShopOwner/:id', async (req, res, next) => {
    const shopOwner = await UserInfo.findById(req.params.id);
    return res.render('shopOwner/approveShopOwner', { shopOwner: shopOwner });
});

router.get('/shopOwnerDetailsDisplay/:id', async (req, res, next) => {
    const shopOwner = await UserInfo.findById(req.params.id);
    return res.render('shopOwner/shopOwnerDetail', { shopOwner: shopOwner });
});

module.exports = router;
