const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.sendFile(process.env.PWD + '/views/auth/join.html');
});

module.exports = router;
