const express = require('express');
const router = express.Router();
const { Category } = require('../../../model/category/categoryInfo');

router.get('/createCategoryDisplay', (req, res, next) => {
    return res.render('category/createCategory');
});

router.get('/updateCategoryDisplay/:id', async (req, res, next) => {
    const category = await Category.findById(req.params.id);
    return res.render('category/updateCategory', { category: category });
});

router.get('/categoryListDisplay', (req, res, next) => {
    return res.render('category/categoryList');
});

module.exports = router;
