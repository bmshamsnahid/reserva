$( document ).ready(function() {
    const token = localStorage.getItem('token');

    if (token) {
        axios.get('/api/authentication/me', {
            headers: {
                "x-auth-token": token
            }
        }).then((response) => {
                if (response.data.success) {
                    const userInfo = response.data.data;
                    if (userInfo.isSuperAdmin) {
                        $('#adminSideNav').css('display', 'inline');
                        $('#shopOwnerSideNav').css('display', 'inline');
                        $('#productSideNav').css('display', 'inline');
                        $('#categorySideNav').css('display', 'inline');
                    } else if (userInfo.isAdmin) {
                        $('#shopOwnerSideNav').css('display', 'inline');
                        $('#productSideNav').css('display', 'inline');
                        $('#categorySideNav').css('display', 'inline');
                    } else if (userInfo.isOwner) {
                        $('#productSideNav').css('display', 'inline');
                    }
                } else {
                    $(location).attr("href", "/view/join");
                }
            })
            .catch(function (error) {
                console.log(error);
                $(location).attr("href", "/view/join");
            });
    } else {
        $(location).attr("href", "/view/join");
    }
});

