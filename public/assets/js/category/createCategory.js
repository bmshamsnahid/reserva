$( document ).ready(function() {
    const createCategoryButton = $('#createCategoryButton');

    createCategoryButton.click(() => {
        const createCategoryName = $('#createCategoryName').val();
        console.log('Name: ' + createCategoryName);
        
        const token = localStorage.getItem('token');
        if (!token) return $(location).attr("href", "/view/join");

        const body = {
            name: createCategoryName,
        };
        const options = {
            headers: {
                'Content-Type': 'application/json',
                "x-auth-token": token
            }
        };

        axios.post('/api/category', body, options)
          .then(function (response) {
            if (response.data.success) {
                toastr.success('Category created successfully.', 'Success')
            }
          })
          .catch(function (error) {
            console.log(error.response.data.message);
            toastr.error(error.response.data.message, 'Error')
          });
    });
});
