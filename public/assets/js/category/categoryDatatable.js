$(document).ready(function () {
    $('#categoryDatatable').dataTable({
        ajax: {
          contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
          url: '/api/category/categoryDataTable',
          type: 'POST',
        },
        serverSide: true,
        columns: [
          { 'data': 'name' },
          {
              "render": function (data, type, full, meta) {
                const editCategoryPageUrl = '/view/category/updateCategoryDisplay/' + full._id;
                return '<a href=' + editCategoryPageUrl + ' class="btn btn-success">Edit</a>';
              }
          }
        ]
      });
});
