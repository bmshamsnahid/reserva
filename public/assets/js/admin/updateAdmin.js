$( document ).ready(function() {
  const updateAdminButton = $('#updateAdminButton');

  updateAdminButton.click(() => {
      const id = $('#id').text();
      const updateAdminName = $('#updateAdminName').val();
      const updateAdminEmail = $('#updateAdminEmail').val();
      const updateAdminPhone = $('#updateAdminPhone').val();

      const token = localStorage.getItem('token');
      if (!token) return $(location).attr("href", "/view/join");

      const body = {
          name: updateAdminName,
          email: updateAdminEmail,
          phone: updateAdminPhone,
          authenticationType: "local",
          isAdmin: true,
      };

      const options = {
          headers: {
              'Content-Type': 'application/json',
              "x-auth-token": token
          }
      };

      axios.patch('/api/userInfo/' + id, body, options)
        .then(function (response) {
          if (response.data.success) {
              console.log(response.data.data);
          }
        })
        .catch(function (error) {
          console.log(error.response);
        });
  });
});
