$(document).ready(function () {
    $('#adminDatatable').dataTable({
        ajax: {
          contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
          url: '/api/admin/get-admin-data',
          type: 'POST',
        },
        serverSide: true,
        columns: [
          { 'data': 'name' },
          { 'data': 'email' },
          {
              "render": function (data, type, full, meta) {
                const url = '/view/admin/updateAdminDisplay/' + full._id;
                return '<a href=' + url + ' class="btn btn-primary">Edit</a>';
              }
          }
        ]
      });
});
