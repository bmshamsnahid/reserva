$( document ).ready(function() {
    let signUpButton = $('#signUpButton');

    signUpButton.click(() => {

        const name = $('#signUpName').val();
        const email = $('#signUpEmail').val();
        const phone = $('#signUpPhone').val();
        const password = $('#signUpPassword').val();

        axios.post('/api/authentication/signUp', {
            name: name,
            email: email,
            phone: phone,
            password: password,
            authenticationType: "local"
          }, {
            headers: {
                'Content-Type': 'application/json',
            }
          })
          .then(function (response) {
            if (response.data.success) {
                localStorage.setItem('token', response.data.token);
                // console.log(response.data);
                $(location).attr("href", "/view/home");
            }
          })
          .catch(function (error) {
            console.log(error.response);
          });
    });
});
