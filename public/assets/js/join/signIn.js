$( document ).ready(function() {
    $.ajaxSetup({ contentType: "application/json; charset=utf-8", });

    let signInButton = $('#signInButton');

    signInButton.click(() => {
        const signInEmail = $('#signInEmail').val();
        const signInPassword = $('#signInPassword').val();

        axios.post('/api/authentication/signIn', {
            email: signInEmail,
            password: signInPassword,
            authenticationType: "local"
          }, {
            headers: {
                'Content-Type': 'application/json',
            }
          })
          .then(function (response) {
            if (response.data.success) {
                localStorage.setItem('token', response.data.data);
                $(location).attr("href", "/view/home");
            }
          })
          .catch(function (error) {
            console.log(error.response);
          });
    });
});
