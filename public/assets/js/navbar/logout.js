$( document ).ready(function() {
    let logOutButton = $('#logOutButton');
    
    logOutButton.click(() => {
        localStorage.setItem('token', null);
        $(location).attr("href", "/view/join");
    });
});
