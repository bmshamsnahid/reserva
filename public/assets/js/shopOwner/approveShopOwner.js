$(document).ready(function () {
    const approvaeShopOwnerButton = $('#approveShopOwner');
    approvaeShopOwnerButton.click(() => {
        console.log('Shop Owner Button Clicked');
        const id = $('#shopOwnerId').text();
        
        const token = localStorage.getItem('token');
        
        if (token) {
            axios.get('/api/shopOwner/shopOwnerApproval/' + id, {
                headers: {
                    "x-auth-token": token
                }
            }).then((response) => {
                    if (response.data.success) {
                        const url = '/view/shopOwner/approveShopOwner/' + id;
                        $(location).attr("href", url);
                        toastr.success('Shop owner approved successfully', 'Approved');
                    } else {
                        
                    }
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        } else {
            $(location).attr("href", "/view/join");
        }
    });
});
