$(document).ready(function () {
    $('#shopOwnerDatatable').dataTable({
        ajax: {
          contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
          url: '/api/shopOwner/shopOwnerDataTable',
          type: 'POST',
        },
        serverSide: true,
        columns: [
          { 'data': 'name' },
          { 'data': 'email' },
          {
              "render": function (data, type, full, meta) {
                
                const approvePageUrl = '/view/shopOwner/approveShopOwner/' + full._id;
                const detailPageUrl = '/view/shopOwner/shopOwnerDetailsDisplay/' + full._id;
                
                if (!full.isApproved) {
                  return '<a href=' + approvePageUrl + ' class="btn btn-success">Approve</a>';
                } else {
                  return '<a href=' + detailPageUrl + ' class="btn btn-info">Details</a>';
                }
              }
          }
        ]
      });
});
