const mongoose = require('mongoose');
const Joi = require('joi');

const productInfoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [ true, 'Name is required'],
        minLength: 1,
        maxLength: 30,
    },
    description: {
        type: String,
        required: [ true, 'Description is required'],
        minLength: 1,
        maxLength: 30,
    },
    quantity: {
        type: Number,
        required: [ true, 'Quantity is required'],
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'category',
        required: [ true, 'Category id is required'],
    },
});

const validateProduct = (product) => {
    const schema = {
        name: Joi.string().min(1).max(30).required(),
        description: Joi.string().min(1).max(30).required(),
        quantity: Joi.number().required(),
        category: Joi.any().required(),
    };

    return Joi.validate(product, schema);
}

const Product = mongoose.model('product', productInfoSchema);

exports.productInfoSchema = productInfoSchema;
exports.validateProduct = validateProduct;
exports.Product = Product;
