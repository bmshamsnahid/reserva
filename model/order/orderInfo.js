const mongoose = require('mongoose');
const Joi = require('joi');

const orderedProductSchema = new mongoose.Schema({
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'product',
        required: [ true, 'Product id is required'],
    },
    productQuantity: {
        type: Number,
        required: [ true, 'Quantity is required'],
    },
});

const orderSchema = new mongoose.Schema({
    buyerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: [ true, 'Buyer id is required'],
    },
    supplierId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: [ true, 'Supplier id is required'],
    },
    products: [orderedProductSchema],
    isSeen: {
        type: Boolean,
        default: false,
    },
    isConfirmed: {
        type: Boolean,
        default: false,
    },
    isPaymentCompleted: {
        type: Boolean,
        default: false,
    },
    isDelivered: {
        type: Boolean,
        default: false,
    },
    isReceived: {
        type: Boolean,
        default: false,
    },
});

const validateOrder = (order) => {
    const schema = {
        buyerId: Joi.any().required(),
        supplierId: Joi.any().required(),
        products: Joi.array().items(),
        isSeen: Joi.boolean(),
        isConfirmed: Joi.boolean()
            .when('isSeen', { is: true, then: Joi.boolean().valid(true, false), otherwise: Joi.boolean().valid(false) }),
        isPaymentCompleted: Joi.boolean()
            .when('isConfirmed', { is: true, then: Joi.boolean().valid(true, false), otherwise: Joi.boolean().valid(false) }),
        isDelivered: Joi.boolean()
            .when('isPaymentCompleted', { is: true, then: Joi.boolean().valid(true, false), otherwise: Joi.boolean().valid(false) }),
        isReceived: Joi.boolean()
            .when('isDelivered', { is: true, then: Joi.boolean().valid(true, false), otherwise: Joi.boolean().valid(false) }),
    };

    return Joi.validate(order, schema);
};

const validateOrderedProduct = (orderedProduct) => {
    const schema = {
        productId: Joi.any().required(),
        productQuantity: Joi.number().required(),
    };

    return Joi.validate(orderedProduct, schema);
};

const Order = mongoose.model('order', orderSchema);

exports.orderSchema = orderSchema;
exports.validateOrder = validateOrder;
exports.Order = Order;

exports.validateOrderedProduct = validateOrderedProduct;
exports.orderedProductSchema = orderedProductSchema;
