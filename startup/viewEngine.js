const expressHbs   = require('express-handlebars');

module.exports = (app) => {
    app.engine('.hbs', expressHbs({
        helpers: {
            foo: function () { return 'FOO!'; },
            equal: function (lvalue, rvalue, options) {
                if (arguments.length < 3)
                    throw new Error("Handlebars Helper equal needs 2 parameters");
                if( lvalue!=rvalue ) {
                    return options.inverse(this);
                } else {
                    return options.fn(this);
                }
            }
        },
        defaultLayout: 'layout',
        extname: '.hbs'
    }));
    app.set('view engine', '.hbs');
};
