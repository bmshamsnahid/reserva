const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const error = require('../middleware/error');

const authenticationRouter = require('../router/auth/authentication');

const superAdminRouter = require('../router/superAdminRouter/superAdminResourceInfo');
const adminRouter = require('../router/adminRouter/adminResourceInfo');
const ownerRouter = require('../router/ownerRouter/ownerResourceInfo');
const categoryRouter = require('../router/category/categoryInfo');
const productRouter = require('../router/product/productInfo');
const orderRouter = require('../router/order/orderInfo');
const userInfoRouter = require('../router/user/userInfo');
const adminInfoRouter = require('../router/admin/adminInfo');
const shopOwnerInfoRouter = require('../router/shopOwner/shopOwnerInfo');

const viewAuthRouter = require('../router/view/auth/join');
const viewHomeRouter = require('../router/view/homePage/home');
const viewAdminRouter = require('../router/view/admin/adminInfo');
const viewProductRouter = require('../router/view/product/productInfo');
const viewShopOwnerRouter = require('../router/view/shopOwner/shopOwnerInfo');
const viewCategoryRouter = require('../router/view/category/categoryInfo');

module.exports = (app) => {
    // app.use(express.json());
    // app.use(bodyParser.json());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(morgan('dev'));

    // api router prefix
    app.use('/api/authentication', authenticationRouter);

    app.use('/api/superAdminTest', superAdminRouter);
    app.use('/api/adminTest', adminRouter);
    app.use('/api/ownerTest', ownerRouter);
    app.use('/api/category', categoryRouter);
    app.use('/api/product', productRouter);
    app.use('/api/order', orderRouter);
    app.use('/api/userInfo', userInfoRouter);
    app.use('/api/admin', adminInfoRouter)
    app.use('/api/shopOwner', shopOwnerInfoRouter);

    // view router prefix
    app.use('/view/join', viewAuthRouter);
    app.use('/view/home', viewHomeRouter);
    app.use('/view/admin', viewAdminRouter);
    app.use('/view/product', viewProductRouter);
    app.use('/view/shopOwner', viewShopOwnerRouter);
    app.use('/view/category', viewCategoryRouter);

    // runtime async error
    app.use(error);
};
