const { productInfoSchema, validateProduct, Product } = require('../../model/product/productInfo');
const _ = require('lodash');

const createProduct = async (req, res, next) => {
    const { error } = validateProduct(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const product = new Product(_.pick(req.body, ['name', 'description', 'quantity', 'category']));
    
    const result = await product.save();

    return res.status(200).json({ success: true, data: result });
};

const getProduct = async (req, res, next) => {
    const product = await Product.findById(req.params.id);
    if (!product) return res.status(400).json({ success: false, message: 'Invalid product id' });

    return res.status(200).json({ success: false, data: product });
};

const getProducts = async (req, res, next) => {
    const products = await Product.find();
    return res.status(200).json({ success: true, data: products });
};

const updateProduct = async (req, res, next) => {
    let product = await Product.findById(req.params.id);
    if (!product) return res.status(400).json({ success: false, message: 'Invalid product id' });

    product.name = req.body.name || product.name;
    product.description = req.body.description || product.description;
    product.quantity = req.body.quantity || product.quantity;
    product.category = req.body.category || product.category;

    const { error } = validateProduct(_.pick(product, ['name', 'description', 'quantity', 'category']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await product.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteProduct = async (req, res, next) => {
    const product = await Product.findByIdAndRemove(req.params.id);
    if (!product) return res.status(400).json({ success: false, message: 'Invalid product id' });

    return res.status(200).json({ success: true, data: product });
};

module.exports = {
    createProduct,
    getProduct,
    getProducts,
    updateProduct,
    deleteProduct,
};
