const { UserInfo, validateUserInfo } = require('../../model/user/userInfo');
const  bcrypt = require('bcrypt');
const _ = require('lodash');

const createUserInfo = async (req, res, next) => {
    const { error } = validateUserInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    let userInfo = await UserInfo.findOne({ email: req.body.email });
    if (userInfo) return res.status(400).json({ success: false, message: 'User already registered' });

    userInfo = new UserInfo(_.pick(req.body, ['name', 'email', 'phone', 'password', 'imageUrl', 'authenticationType', 'googleId', 'facebookId', 'isSuperAdmin', 'isAdmin', 'isOwner' ]));

    if (req.body.authenticationType === 'local') {
        const salt = await bcrypt.genSalt(10);
        userInfo.password = await bcrypt.hash(userInfo.password, salt);
    }

    await userInfo.save();

    return res.status(200).json({ success: true, data: _.pick(userInfo, ['_id', 'name', 'email']) });
};

const getUserInfo = async (req, res, next) => {
    const userInfo = await UserInfo.findById(req.params.id);
    if(!userInfo) return res.status(400).json({ success: false, message: 'Invalid user id' });

    return res.status(200).json({ success: true, data: userInfo });
};

const getUserInfos = async (req, res, next) => {
    const userInfos = await UserInfo.find();

    return res.status(200).json({ success: true, data: userInfos });
};

const updateUserInfo = async (req, res, next) => {
    const userInfo = await UserInfo.findById(req.params.id);
    if(!userInfo) return res.status(400).json({ success: false, message: 'Invalid user id' });

    userInfo.name = req.body.name || userInfo.name;
    userInfo.email = req.body.email || userInfo.email;
    userInfo.phone = req.body.phone || userInfo.phone;

    const { error } = validateUserInfo(_.pick(userInfo, ['name', 'email', 'phone', 'password', 'imageUrl', 'authenticationType', 'googleId', 'facebookId', 'isSuperAdmin', 'isAdmin', 'isOwner', 'isApproved' ]));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await userInfo.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteUserInfo = async (req, res, next) => {
    const userInfo = await UserInfo.findByIdAndRemove(req.params.id);
    if (!userInfo) return res.status(400).json({ success: false, message: 'Invalid user id' });

    return res.status(200).json({ success: true, data: userInfo });
};

module.exports = {
    createUserInfo,
    getUserInfo,
    getUserInfos,
    updateUserInfo,
    deleteUserInfo
};
