const { 
    validateOrder, 
    Order, 
    validateOrderedProduct 
} = require('../../model/order/orderInfo');
const _ = require('lodash');

const createOrder = async (req, res, next) => {
    const { error } = validateOrder(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });
    
    const { productError } = await checkOrder(req.body.products);
    if (productError) return res.status(400).json({ success: false, message: productError });
    
    const order = new Order(_.pick(req.body, ['buyerId', 'supplierId', 'products', 'isSeen', 'isConfirmed', 'isPaymentCompleted', 'isDelivered', 'isReceived']));

    const result = await order.save();
    
    return res.status(200).json({ success: true, data: result });
};

const getOrder = async (req, res, next) => {
    const order = await Order.findById(req.params.id);
    if (!order) return res.status(400).json({ success: false, message: 'Invalid order id' });

    return res.status(200).json({ success: true, data: order });
};

const getOrders = async (req, res, next) => {
    const orders = await Order.find();
    return res.status(200).json({ success: true, data: orders });
};

const updateOrder = async (req, res, next) => {
    let order = await Order.findById(req.params.id);
    if (!order) return res.status(400).json({ success: false, message: 'Invalid order id' });

    order.buyerId = req.body.buyerId || order.buyerId;
    order.supplierId = req.body.supplierId || order.supplierId;
    order.products = req.body.products || order.products;
    order.isSeen = req.body.isSeen || order.isSeen;
    order.isConfirmed = req.body.isConfirmed || order.isConfirmed;
    order.isPaymentCompleted = req.body.isPaymentCompleted || order.isPaymentCompleted;
    order.isDelivered = req.body.isDelivered || order.isDelivered;
    order.isReceived = req.body.isReceived || order.isReceived;
    
    const { error } = validateOrder(_.pick(order, ['buyerId', 'supplierId', 'products', 'isSeen', 'isConfirmed', 'isPaymentCompleted', 'isDelivered', 'isReceived']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const { productError } = await checkOrder(order.products);
    if (productError) return res.status(400).json({ success: false, message: productError });
   
    const result = await order.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteOrder = async (req, res, next) => {
    const order = await Order.findByIdAndRemove(req.params.id);
    if (!order) return res.status(400).json({ success: false, message: 'Invalid order id' });

    return res.status(200).json({ success: true, data: order });
};

module.exports = {
    createOrder,
    getOrder,
    getOrders,
    updateOrder,
    deleteOrder,
};

const checkOrder = async (products) => {
    if (typeof products == 'undefined' || products.length <= 0) return { productError: "Products does not exist" };

    for (let index=0; index<products.length; index++) {
        const product = products[index];
        const { error } = validateOrderedProduct(_.pick(product, ['productId', 'productQuantity']));
        if (error) return { productError: error.details[0].message }
    }
    return { productError: null }
}
