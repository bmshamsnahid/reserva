const { UserInfo } = require('../../model/user/userInfo');

const getAllAdmin = async (req, res, next) => {
    const admins = await UserInfo.find({ isAdmin: true });
    return res.status(200).json({ susccess: true, data: admins });
};

const getAdminPagination = async (req, res, next) => {
    
    const skip = parseInt(req.params.skip);
    const limit = parseInt(req.params.limit);

    const admins = await UserInfo.find({ isAdmin: true })
        .sort({ name: 1 })
        .skip(skip)
        .limit(limit);
    
    return res.status(200).json({ susccess: true, data: admins });
};

const adminCount = async (req, res, next) => {
    const adminCount = await UserInfo.find({ isAdmin: true }).count();

    return res.status(200).json({ success: true, data: adminCount });
};

const searchAdmin = async (req, res, next) => {
    const admins = await UserInfo.find()
        .or([{ 
                name: {
                    $regex : ".*" + req.params.keyword + ".*"
                } 
            },
        ]);;
    return res.status(200).json({ susccess: true, data: admins });
};

const getAllAdminData = async (req, res, next) => {

    UserInfo.dataTables({
        limit: req.body.length,
        skip: req.body.start,
        find: { isAdmin: true },
        search: {
            value: req.body.search.value,
            fields: ['name', 'email']
          },
        order: req.body.order,
        columns: req.body.columns
      }).then(function (table) {
        res.json({
          data: table.data,
          recordsFiltered: table.total,
          recordsTotal: table.total
        });
      });
};

module.exports = {
    getAllAdmin,
    getAdminPagination,
    adminCount,
    searchAdmin,
    getAllAdminData,
};
