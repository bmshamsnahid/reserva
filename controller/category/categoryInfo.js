const { categorySchema, validateCategory, Category } = require('../../model/category/categoryInfo');
const _ = require('lodash');

const createCategory = async (req, res, next) => {
    const { error } = validateCategory(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    // check if a category already exist
    let category = await Category.findOne({ name: req.body.name });
    if (category) return res.status(400).json({ success: false, message: 'Category already exist.' });

    category = new Category(_.pick(req.body, ['name']));

    const result = await category.save();

    return  res.status(200).json({ success: true, data: result });
};

const getCategory = async (req, res, next) => {
    const category = await Category.findById(req.params.id).select('_id', 'name');
    if (!category) return res.status(400).json({ success: false, message: 'Invalid category id' });

    return res.status(200).json({ success: true, data: category });
};

const getCategories = async (req, res, next) => {
    const categories = await Category.find().select('_id name');
    return res.status(200).json({ success: true, data: categories });
};

const updateCategory = async (req, res, next) => {
    let category = await Category.findById(req.params.id).select('-_v');

    if (!category) return res.status(400).json({ success: false, message: 'Invalid category id' });

    const existingCategory = await Category.findOne({ name: category.name });
    if (existingCategory) return res.status(400).json({ success: false, message: 'Category already exists' });

    category.name = req.body.name || category.name;

    const { error } = validateCategory(_.pick(req.body, ['name']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await category.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteCategory = async (req, res, next) => {
    const result = await Category.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).json({ success: false, message: 'Invalid category id' });

    return res.status(200).json({ success: true, data: result });
};

const getAllCategoryData = async (req, res, next) => {

    Category.dataTables({
        limit: req.body.length,
        skip: req.body.start,
        find: {},
        search: {
            value: req.body.search.value,
            fields: ['name']
          },
        order: req.body.order,
        columns: req.body.columns
      }).then(function (table) {
        res.json({
          data: table.data,
          recordsFiltered: table.total,
          recordsTotal: table.total
        });
      });
};

module.exports = {
    createCategory,
    getCategory, 
    getCategories,
    updateCategory,
    deleteCategory,
    getAllCategoryData,
};
