module.exports = (err, req, res, next) => {
    console.log(new Error(err));
    return res.status(500).json({ success: false, message: 'Internal Server Error: ' + err.message, error: err });
};
