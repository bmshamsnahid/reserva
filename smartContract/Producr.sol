pragma solidity ^0.5.0;
contract Product
{
    address public Owner;
    string public Name;
    string public GenericName;
    string public Description;
    int public Quantity;
    int public AskingPrice;
    mapping(address => int) public OrderPriceMap;
    mapping(address => int) public OrderQuantityMap;
    constructor(string memory description, int price, int quantity) public
    {
        Owner = msg.sender;
        AskingPrice = price;
        Description = description;
        Quantity = quantity;
    }
    function OrderProducts(int orderPrice, int orderQuantity) public
    {
        if (orderPrice == 0)
        {
            revert();
        }
        if (Quantity < orderQuantity)
        {
            revert();
        }
        
        if (Owner == msg.sender)
        {
            revert();
        }
        OrderPriceMap[msg.sender] = orderPrice;
        OrderQuantityMap[msg.sender] = orderQuantity;
    }
    function Reject(address orderId) public
    {
        if (Owner != msg.sender)
        {
            revert();
        }
        OrderPriceMap[orderId] = 0;
        OrderQuantityMap[orderId] = 0;
    }
    function AcceptOffer(address orderId) public
    {
        if ( msg.sender != Owner )
        {
            revert();
        }
        if ( Quantity < OrderQuantityMap[orderId] )
        {
            revert();
        }
        Quantity = Quantity - OrderQuantityMap[orderId];
        OrderPriceMap[orderId] = 0;
        OrderQuantityMap[orderId] = 0;
    }
    function IncreseQuantity(int increseBy) public
    {
        if ( msg.sender != Owner )
        {
            revert();
        }
        Quantity = Quantity + increseBy;
    }
    function DecreseQuantity(int decreseBy) public
    {
        if ( msg.sender != Owner )
        {
            revert();
        }
        Quantity = Quantity + decreseBy;
    }
}